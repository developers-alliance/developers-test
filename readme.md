# Backend Developer Test

> Please note that this is a fictional client and not a specific client requirement. 
> The scenario is meant to serve as a basic technical test to help us gauge your level of experience using Magento.
> If you cannot complete the task then please present the code you wrote and then an explanation of how you would have completed any remaining tasks.

## Client Brief

We have been asked by a fictional client to create a custom extension for a Magento store, called Linkedin, within the Alliance namespace. The purpose of the module is to require customers to also provide a link to their Linkedin profile when creating a new account.

## Technical Requirements

- A new customer attribute is created called 'linkedin_profile'
- The new attribute should be included on the front end registration forms, including registration during checkout, and the admin forms (registration and checkout). 
- The new attribute should also be editable in the customer form for admin users in the Magento back office interface and for customers in the 'My Account' section of Magento
- Whether the field is invisible/optional/required in the forms should be configurable from the admin and enforced both client and server side
- The attribute value should be validated for being unique for all customers, a valid url and having a maximum length of 250 characters
- The attribute value should also be persisted when doing a guest checkout
- The attribute should be included in the SOAP API (V1 & V2, including WSI) customer related operations (e.g. list customer info, create customer, checkout)

## Constraints

- The module should be implemented in the latest *Magento Community* edition
- The module should be installable through composer hosted in a private bitbucket repository
- No skinning is required, however we do expect to receive theming assets (templates, layout, ..) applicable when using the RWD theme
- Should not use a third party module
- Should adhere to Zend Framework Coding standards. E.g. 4 spaces for tabs. Any code not adhering to standards will simply be ignored
- Should follow Magento conventions as closely as possible
- We don't just expect the module to satisfy the requirements, a clean solution which applies common Magento development patterns and architecture is what we look for 

Good luck!

##### Disclaimer
Candidates are entirely responsible for their own time and costs associated in completing the technical test